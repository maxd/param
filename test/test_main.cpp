
#include <Param.h>
#include <ParamJson.h>
#include <ArduinoJson.h>
#include <unity.h>
#include "FastLED.h"

// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;

EnumParam aEnumParam;
enum THINGS {
    LABRADOR = 2,
    SHIBA = 5,
    BEAGLE,
    GOLDENBOI = 128,
    HUSKY = 4,
    BORDER_COLLIE
};
const int enumValues[] = {LABRADOR, SHIBA, BEAGLE, GOLDENBOI, HUSKY, BORDER_COLLIE};
const char* enumStrings[] = {"labrador", "shiba", "beagle", "golden boi", "husky", "border collie"};


// and a param collector
ParamCollector paramCollector;

#define LED_PIN 13
#define JSON_BUFFER_SIZE 1024



void receiveTestString(const char * _string) {
    StaticJsonDocument<JSON_BUFFER_SIZE> doc;
    DeserializationError err = deserializeJson(doc, _string);
    if(err){
        Serial.printf("json fail :%s", err.c_str());
    }
    else {
        // receive the message
        JsonObject _obj = doc.as<JsonObject>();
        receiveJson(_obj, paramCollector);
    }
}


/////////////////////////////////////////////////////////////
///      tests
/////////////////////////////////////////////////////////////

void test_setByteValue_IntParam(){
    aIntParam.setWithByteValue(42);
    TEST_ASSERT_EQUAL(42, aIntParam.v);
}

void test_setByteValue_FloatParam(){
    aFloatParam.setWithByteValue(128);
    TEST_ASSERT_EQUAL(0.5, aFloatParam.v);
}

void test_setByteValue_EnumParam(){
    aEnumParam.setWithByteValue(23);
    TEST_ASSERT_EQUAL(enumValues[2], aEnumParam.v);
}

///////////////////////////////////////////////////////////



void test_osc_input(){

}

void test_string_input_float(){
    const char* t = "{\"addr\":\"/a/float\",\"v\":\"0.96231078158789\"}";
    receiveTestString(t);
    TEST_ASSERT_EQUAL(0.96231078158789, aFloatParam.v);
}

void test_string_input_int(){
    const char* t = "{\"addr\":\"/a/int\",\"v\":\"137\"}";
    receiveTestString(t);
    TEST_ASSERT_EQUAL(137, aIntParam.v);
    // TEST_ASSERT_EQUAL(142, aIntParam.v);
}

void test_string_input_color(){
    // color r 100 g 100 b 100
    //(should be more like 42 64 69 to test color order)
    const char* t = "{\"addr\":\"/a/color\",\"v\":6579300}";
    receiveTestString(t);
    CRGB c = aColorParam.v;
    TEST_ASSERT_EQUAL(100, c.r);
    TEST_ASSERT_EQUAL(100, c.g);
    TEST_ASSERT_EQUAL(100, c.b);
}

void test_string_input_bang(){
    const char* t = "{\"addr\":\"/a/bang\",\"v\":\"\"}";
    aBangParam.checkFlag();
    TEST_ASSERT_EQUAL(false, aBangParam.flag);
    receiveTestString(t);
    TEST_ASSERT_EQUAL(true, aBangParam.flag);
}

void test_string_input_bool(){
    const char* t = "{\"addr\":\"/a/bool\",\"v\":1}";
    aBoolParam.setBoolValue(false);
    TEST_ASSERT_EQUAL(false, aBoolParam.v);
    receiveTestString(t);
    TEST_ASSERT_EQUAL(true, aBoolParam.v);
}

void test_string_input_text(){
    const char* t = "{\"addr\":\"/a/text\",\"v\":\"yes indeed\"}";
    receiveTestString(t);
    TEST_ASSERT_EQUAL("yes indeed", aTextParam.v);
}

void test_string_input_enum(){
    const char* t = "{\"addr\":\"/a/enum\",\"v\":\"golden boi\"}";
    receiveTestString(t);
    TEST_ASSERT_EQUAL(enumValues[3], aEnumParam.v);
}

void bangFunction(){
    Serial.println("ahah");
}

void ledCallback(bool _i){
    digitalWrite(LED_PIN, _i);
}

void setup(){
    Serial.begin(9600);
    while(!Serial);
    // Serial.println("connected via serial");
    // delay(2000);
    UNITY_BEGIN();

    pinMode(LED_PIN, OUTPUT);
    // set parameters
    aFloatParam.set("/a/float", 0, 1, 0.5);
    aIntParam.set("/a/int", 0, 255, 100);
    aColorParam.set("/a/color", 0);
    aBangParam.set("/a/bang");
    aBoolParam.set("/a/bool", 0);
    aTextParam.set("/a/text", "foo");
    aEnumParam.set("/a/enum", enumStrings, enumValues, 6, SHIBA);

    // aBoolParam.setCallback(ledCallback);
    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);

    RUN_TEST(test_setByteValue_IntParam);
    RUN_TEST(test_setByteValue_FloatParam);
    RUN_TEST(test_setByteValue_EnumParam);
    Serial.println();
    RUN_TEST(test_string_input_float);
    RUN_TEST(test_string_input_int);
    RUN_TEST(test_string_input_bang);
    RUN_TEST(test_string_input_bool);
    RUN_TEST(test_string_input_color);
    // RUN_TEST(test_string_input_text);
    RUN_TEST(test_string_input_enum);

    UNITY_END();
}


void loop(){
    // checkParamsSerial();
}
