/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.0.1
 * @since     2020-02-11
 */


#include "ParamMenu.h"


void ParamMenu::begin(ParamCollector _collector) {
    allItems[0].parent = NULL;
    allItems[0].previous = NULL;
    allItems[0].next = NULL;
    allItems[0].child = NULL;
    strcpy(allItems[0].name,"root");
    // allItems[0].name = "root";
    root = &allItems[0];
    index = 1;

    for(int i = 0; i < _collector.index; i++) {
        addParam(_collector.pointers[i]);
    }

    // for(int i = 0; i < index; i++){
    //     Serial.printf("---- %s ----\n", allItems[i].name);
    //     if(allItems[i].parent != NULL) Serial.printf("parent %s \n", allItems[i].parent->name);
    //     if(allItems[i].previous != NULL) Serial.printf("previous %s \n", allItems[i].previous->name);
    //     if(allItems[i].next != NULL) Serial.printf("next %s \n", allItems[i].next->name);
    //     if(allItems[i].child != NULL) Serial.printf("child %s \n", allItems[i].child->name);
    // }
    selectedItem = allItems[0].child;
    update = true;
}


// add parameter to menu system
// dynamicaly creates a "weaping willow tree"
// items have pointers to previous/next parent/child if applicable
// root | Item ---------------- | Item
//      | Item ----- | Item     | Item ---- | Item
//      | Item -...  | Item     | Item      | Item
//      | Item       | ...      | ...       | Item
//

void ParamMenu::addParam(Param * _p) {
    // these steps tokenizes the address
    size_t length = strlen(_p->address);
    char * copy = new char[length+1];
    strncpy(copy, _p->address, length+1);
    char * split = strtok(copy, "/");
    // start at the root
    MenuItem * item = &allItems[0];


    while (split != NULL){
        // if child null, add item as new level
        if(item->child == NULL){
            size_t len = strlen(split);
            len = (len < PARAM_MENU_MAX_NAME_LENGTH) ? len : PARAM_MENU_MAX_NAME_LENGTH;
            strncpy(allItems[index].name, split, len);
            item->child = &allItems[index];
            allItems[index].parent = item;
            // step
            item = &allItems[index];
            index++;
        }
        else {
            // step one level
            item = item->child;
            if(strcmp(item->name, split) != 0){
                bool found = false;
                // traverse siblings
                while(item->next != NULL){
                    if(strcmp(item->name, split) == 0){
                        found = true;
                        break;
                    }
                    else {
                        item = item->next;
                    }
                }
                if(!found) {
                    size_t len = strlen(split);
                    len = (len < PARAM_MENU_MAX_NAME_LENGTH) ? len : PARAM_MENU_MAX_NAME_LENGTH;
                    strncpy(allItems[index].name, split, len);
                    item->next = &allItems[index];
                    allItems[index].previous = item;
                    // step
                    item = &allItems[index];
                    index++;
                }
            }
        }
        // tokenLength = strlen(split);
        split = strtok (NULL, "/");
    }
    // add param to final MenuItem
    item->param = _p;
}

void ParamMenu::up(){
    update = true;
    if(selectedItem->param != NULL) {
        // increment param
    }
    else if(selectedItem->previous != NULL) {
        selectedItem = selectedItem->previous;
    }
}

void ParamMenu::down(){
    update = true;
    if(selectedItem->param != NULL) {
        // decrement param
    }
    else if(selectedItem->next != NULL) {
        selectedItem = selectedItem->next;
    }
}

void ParamMenu::enter(){
    update = true;
    if(selectedItem->child != NULL) {
        selectedItem = selectedItem->child;
    }
    else if(selectedItem->param != NULL) {
        // back();
    }
}

void ParamMenu::reset() {
    update = true;
    selectedItem = root;
}

////////////////////////////////////////////////////////////////


void MenuRenderer::begin(int w, int h, Print * p) {
    width = w;
    height = h;
    printer = p;

}

void MenuRenderer::render(ParamMenu menu){
    MenuItem * mi = menu.selectedItem;
    if(menu.update) {
        menu.update = false;
        for(int i = 0; i < width; i++) printer->print('-');
        printer->println();
        for(int i = 0; i < height; i++){
            if(mi != NULL) {
                printer->println(mi->name);
                mi = mi->next;
            }
        }
    }
}

// traverse menu tree and print it out
void MenuRenderer::recursivePrint(MenuItem * item, int depth) {
    for(int i = 0; i < depth; i++){printer->print("-");}
    printer->print(item->name);
    if(item->param != NULL) {
        printer->printf(" = %s \n", item->param->address);
    }
    else printer->println();

    if(item->child != NULL) {
        recursivePrint(item->child, depth+2);
    }
    if(item->next != NULL) {
        recursivePrint(item->next, depth);
    }
    if(item->child==NULL && item->next ==NULL) return;
}

void MenuRenderer::printOutMenu(ParamMenu menu) {
    MenuItem * it = &menu.allItems[0];
    recursivePrint(it, 1);
}
