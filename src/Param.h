/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */


#ifndef PARAMS_H
#define PARAMS_H

#include "Arduino.h"
// #include "ArduinoJson.h"
#ifndef TEXT_PARAM_MAX_LENGTH
    #define TEXT_PARAM_MAX_LENGTH 32
#endif

typedef enum {
    GENERIC,
    BOOL_PARAM,
    FLOAT_PARAM,
    INT_PARAM,
    COLOR_PARAM,
    TEXT_PARAM,
    ENUM_PARAM,
    BANG_PARAM
} ParamType;

enum INPUT_TYPE {
    TOGGLE,
    I_SLIDER,
    F_SLIDER,
    NUMBER,
    COLOR,
    TEXT,
    OPTION,
    BANG
};

const char INPUT_TYPE_STRINGS[8][10] = {"toggle","i_slider","f_slider","number","color","text","option","bang"};

enum SAVING_OPTIONS {
    NEVER_SAVE,
    SAVE_ON_REQUEST
    // SAVE_ON_CHANGE
};

typedef const char* ParamAddress;
#define OPTION_NAME_MAX_LENGTH 48
// for EnumParam we need a option type

typedef struct {
    const char* name;
    // stuck between these two, I how clean it is to setup with the above
    // but need the later to be dynamic.
    //   char name[OPTION_NAME_MAX_LENGTH];
    int value;
} EnumOption;

//////////////////////////////////////////////////////////////////////////////
/**
 * @class Param
 * @brief Abstract base class for a param
 *
 * Provides a interface to manage parameters of different types. All other
 * parameters are derived from this.
 * 
 */
class Param {
    public:
       /**
        * @brief Setup the parameter
        *
        * Abstract method to setup the parameter with an address 
        * and other data based on the param type, like min/max/def.
        */
        void set(ParamAddress _address);
        
       /**
        * @brief Set a callback
        *
        * When a param changes value it will call this optionnal callback.
        *
        * @param _callback callback to call on value change
        */
        void setCallback(void (*_callback)()) {
            callback = _callback;
        }

       /**
        * @brief Check if address matches.
        *
        * When a param changes value it will call this optionnal callback.
        *
        * @param _address address to match
        * @return true if address matches
        */
        bool match(ParamAddress address);

       /**
        * @brief Check the value changed flag
        *
        * When a param changes value it sets a flag that can be checked with this method.
        * Will set the flag to false when checking.
        *
        * @return true if value changed
        */
        bool checkFlag();

       /**
        * @brief Check the value changed by comparing previous value
        *
        * Does a comparison between `v` and `previous`. Needs to be virtual for `TextParam`.
        *
        * @return true if value changed
        */
        virtual bool hasChanged();

        // interface
        virtual void setBoolValue(bool _i);
        virtual void setFloatValue(float _i);
        virtual void setIntValue(int _i);
        virtual void setColorValue(long _i);
        virtual void setTextValue(const char* _i);
        virtual void setEnumValue(int _i);
        virtual void setBangValue();

       /**
        * @brief Set value with byte
        *
        * Safe ish input of a byte value for the type.
        *
        * @param _v byte value input
        */
        virtual void setWithByteValue(uint8_t _v);

       /**
        * @brief Receive a value as a byte and map it
        *
        * Map a generic byte value to the type. Each type will decide how to map the byte's value
        * to its own value.
        *
        * @param _v byte value input
        */
        virtual void receiveByte(uint8_t _v);

       /**
        * @brief Receive a bang and do something
        *
        * Generic trigger action. Each type will decide what to do with the bang. For example,
        * number params will increment their value and wrap when max is reached, bool params 
        * will toggle true and false.
        */
        virtual void receiveBang();

       /**
        * @brief Receive a boolean and apply it
        *
        * Map a boolean to the value. Each type will decide how to map the boolean value
        * to its own value. For example, number params will toggle between min and default.
        *
        * @param _v byte value input
        */
        virtual void receiveBool(bool _b);

       /**
        * @brief get the value as a byte
        *
        * Get a byte representation of the value. Each param will map its value to a byte.  
        *
        * @return byte representation of value
        */
        virtual uint8_t getByteValue();

       /**
        * @brief get the type of param
        *
        * Used to determin what kind of param this is so that it can be cast to the correct type
        * when needed.
        *
        * @return the type of parameter
        */
        virtual ParamType getParamType(){return paramType;};

        ParamAddress address;
        ParamType paramType;

        uint8_t inputType = TOGGLE;
        uint8_t saveType = NEVER_SAVE;
        bool flag;
        bool readAccess = true;
        bool writeAccess = true;
        void (*callback)();
};


//////////////////////////////////////////////////////////////////////////////

class BoolParam : public Param {
public:
    void set(ParamAddress _address, bool def);
    virtual void setBoolValue(bool _i);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _v);
    
    virtual uint8_t getByteValue();

    virtual void receiveByte(uint8_t _v);
    virtual void receiveBang();
    virtual void receiveBool(bool _b);

    void (*callback)(bool);
    void setCallback(void (*_callback)(bool)) {
        callback = _callback;
    }

    bool v;
    bool previous;
    bool def;
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};

};
//////////////////////////////////////////////////////////////////////////////

class FloatParam : public Param {
public:
    void set(ParamAddress _address, float min, float max, float def);
    virtual void setFloatValue(float _i);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _v);
    virtual uint8_t getByteValue();

    virtual void receiveByte(uint8_t _v);
    virtual void receiveBang();
    virtual void receiveBool(bool _b);

    void (*callback)(float _f);
    void setCallback(void (*_callback)(float _f)) {
        callback = _callback;
    }

    float v; // value
    float previous;
    float min;
    float max;
    float def;
    uint8_t uiType;
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};

};
//////////////////////////////////////////////////////////////////////////////

class IntParam : public Param {
public:
    void set(ParamAddress _address, int min, int max, int def);
    virtual void setIntValue(int _i);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _v);
    virtual uint8_t getByteValue();

    virtual void receiveByte(uint8_t _v);
    virtual void receiveBang();
    virtual void receiveBool(bool _b);
    
    void (*callback)(int _i);
    void setCallback(void (*_callback)(int _i)) {
        callback = _callback;
    }

    int v;
    int previous;
    int min;
    int max;
    int def;
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};

};


//////////////////////////////////////////////////////////////////////////////

class ColorParam : public Param {
public:
    void set(ParamAddress _address, long def);
    virtual void setColorValue(long _i);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _v);
    virtual uint8_t getByteValue();

    void (*callback)(long _c);
    void setCallback(void (*_callback)(long _c)) {
        callback = _callback;
    }
    void setRed(int _c);
    void setGreen(int _c);
    void setBlue(int _c);

    // virtual void receiveByte(uint8_t _v);
    // virtual void receiveBang();
    // virtual void receiveBool(bool _b);

    long v;
    long previous;
    long def;
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};
};
//////////////////////////////////////////////////////////////////////////////


class TextParam : public Param {
public:
    void set(ParamAddress _address, const char* def);
    virtual void setTextValue(const char* _v);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _v);
    virtual uint8_t getByteValue();

    void (*callback)(const char* _t);
    void setCallback(void (*_callback)(const char* _t)) {
        callback = _callback;
    }
    char v[TEXT_PARAM_MAX_LENGTH];
    char previous[TEXT_PARAM_MAX_LENGTH];
    char def[TEXT_PARAM_MAX_LENGTH];
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};
};

//////////////////////////////////////////////////////////////////////////////

// options should be comma seperated "RGB,BGR,GRB,RBG"
class EnumParam : public Param {
public:
    void set(ParamAddress _address, int _def);
    void setOptions(EnumOption * _options, size_t _s);
    virtual void setEnumValue(int _v);
    virtual void setTextValue(const char* _i);
    virtual bool hasChanged();
    virtual void setWithByteValue(uint8_t _i);
    virtual uint8_t getByteValue();

    virtual void receiveByte(uint8_t _v);
    virtual void receiveBang();
    virtual void receiveBool(bool _b);

    void (*callback)(int);
    void setCallback(void (*_callback)(int)) {
        callback = _callback;
    }

    int v;
    int previous;
    size_t index;
    EnumOption * options;

    int optionCount;
    int def; // as value not index
    size_t defIndex;
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};

};
//////////////////////////////////////////////////////////////////////////////


class BangParam : public Param {
public:
    void set(ParamAddress address);
    virtual void setBangValue();
    virtual void setWithByteValue(uint8_t _v);
    virtual uint8_t getByteValue();
    virtual void receiveByte(uint8_t _v);
    virtual void receiveBang();
    virtual void receiveBool(bool _b);
    void (*callback)();
    void setCallback(void (*_callback)()){
        callback = _callback;
    }
    ParamType paramType;
    virtual ParamType getParamType(){return paramType;};

};

//////////////////////////////////////////////////////////////////////////////

// template <class T>
// class NumParam : public Param {
// public:
//     void set(ParamAddress _address, T min, T max, T def);
//
//     void (*callback)(T _i);
//     void setCallback(void (*_callback)(T _i));
//
//     T v;
//     T min;
//     T max;
//     T def;
//     ParamType paramType = FLOAT_PARAM;
//
// };

//////////////////////////////////////////////////////////////////////////////

// what a array param could look like
// class ArrayParam : public Param {
// public:
//     void set(ParamAddress _address, int * _array, int _size);
//     void (*callback)();
//     void setCallback(void (*_callback)()) {
//         callback = _callback;
//     }
//     ParamType paramType;
//     virtual ParamType getParamType(){return paramType;};
// }

#endif
