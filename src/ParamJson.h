/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#ifndef PARAMJSON_H
#define PARAMJSON_H

#include "Param.h"
#include "ParamCollector.h"

#include "ArduinoJson.h"
#include <Arduino.h>

/**
 * @brief Serialize a param into a json object
 * 
 * @param _jsonParam json object to serialize into
 * @param _p param to serialize
 * @param _verbose if false only serialize address and value, 
 *                 if true also serialize attributes.
 * @return json object  
 */
JsonObject jsonifyParam(JsonObject _jsonParam, Param * _p, bool _verbose);
/**
 * @brief Serialize a param into a json object
 * 
 * @param _jsonParam json object to serialize into
 * @param _p param to serialize
 * @param _verbose if false only serialize address and value, 
 *                 if true also serialize attributes.
 * @param _all if true force serilization of all params reguardless of read access
 * @return json object  
 */
JsonObject jsonifyParam(JsonObject _jsonParam, Param * _p, bool _verbose, bool _all);
/**
 * @brief Method to receive JSON input
 * 
 * @param _json json object to receive
 * @param _collector ParamCollector to dispatch to
 */
void receiveJson(JsonObject _json, ParamCollector * _collector);
/**
 * @brief Dispatch JSON input to params
 * 
 * @param _addr ParamAddress
 * @param _v json object
 * @param _collector ParamCollector to dispatch intput to
 * @return true if param was found
 */
bool dispatchJson(ParamAddress _addr, JsonVariant _v, ParamCollector * _collector);
/**
 * @brief Save params into a JsonDocument
 * 
 * @param _json jsonDocument to save into
 * @param _collector ParamCollector to save
 */
void saveParams(JsonDocument &_doc, ParamCollector * _collector);
/**
 * @brief Load params from a JsonDocument
 * 
 * @param _json jsonDocument to load from
 * @param _collector ParamCollector to load into
 */
void loadParams(JsonDocument &_doc, ParamCollector * _collector);

#endif
