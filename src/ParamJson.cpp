/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#include "ParamJson.h"

// static document for all arduino json operations
// StaticJsonDocument<PARAM_JSON_BUFFER_SIZE> paramJsonDoc;

JsonObject jsonifyParam(JsonObject _jsonParam, Param * _p, bool _verbose) {
    return jsonifyParam(_jsonParam, _p, _verbose, false);
}

JsonObject jsonifyParam(JsonObject _jsonParam, Param * _p, bool _verbose, bool _all) {
    _jsonParam["addr"] = _p->address;
    if(_verbose){
        _jsonParam["type"] = INPUT_TYPE_STRINGS[_p->inputType];
    }

    switch(_p->getParamType()) {
        case BOOL_PARAM:
        {
            BoolParam * _ip = (BoolParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _ip->v;
            else _jsonParam["v"] = false;
            if(_verbose){
                _jsonParam["def"] = _ip->def;
            }
        }
        break;
        case FLOAT_PARAM:
        {
            FloatParam * _fp = (FloatParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _fp->v;
            else _jsonParam["v"] = 0.0;
            if(_verbose){
                _jsonParam["min"] = _fp->min;
                _jsonParam["max"] = _fp->max;
                _jsonParam["def"] = _fp->def;
            }
        }
            break;
        case INT_PARAM:
        {
            IntParam * _ip = (IntParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _ip->v;
            else _jsonParam["v"] = 0;
            if(_verbose){
                _jsonParam["min"] = _ip->min;
                _jsonParam["max"] = _ip->max;
                _jsonParam["def"] = _ip->def;
            }
        }
            break;

        case COLOR_PARAM:
        {
            ColorParam * _ip = (ColorParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _ip->v;
            else _jsonParam["v"] = 0;
            if(_verbose){
                _jsonParam["def"] = _ip->def;
            }
        }
            break;

        case TEXT_PARAM:
        {
            TextParam * _ip = (TextParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _ip->v;
            else _jsonParam["v"] = "";
            if(_verbose) {
                _jsonParam["def"] = _ip->def;
            }
        }
            break;

        case ENUM_PARAM:
        {
            EnumParam * _ep = (EnumParam*)_p;
            if(_p->readAccess || _all) _jsonParam["v"] = _ep->options[_ep->index].name;
            else _jsonParam["v"] = _ep->options[0].name;
            if(_verbose) {
                _jsonParam["def"] = _ep->options[_ep->defIndex].name;
                JsonArray ar = _jsonParam.createNestedArray("options");
                for(int i = 0; i < _ep->optionCount; i++) {
                    ar.add(_ep->options[i].name);
                }
            }
        }
            break;
        case BANG_PARAM:
            // no extra elements needed
            break;
        case GENERIC:
            // no extra elements needed
            break;
    }

    return _jsonParam;
}

bool dispatchJson(ParamAddress _addr, JsonVariant _v, ParamCollector * _collector) {
    bool _used = false;
    for(int i = 0; i < _collector->index; i++) {
        Param* _p = _collector->pointers[i];
        // check validity
        if(_p->match(_addr) && _p->writeAccess == true) {
            switch(_p->getParamType()) {
                case BOOL_PARAM :
                {
                    bool v = _v;
                    _p->setBoolValue(v);
                }
                    break;
                case FLOAT_PARAM :
                {
                    float v = _v;
                    _p->setFloatValue(v);
                }
                    break;
                case INT_PARAM :
                {
                    int v = _v;
                    _p->setIntValue(v);
                }
                    break;
                case COLOR_PARAM :
                {
                    long v = _v;
                    _p->setColorValue(v);
                }
                    break;
                case TEXT_PARAM :
                {
                    _p->setTextValue(_v.as<const char*>());
                }
                    break;
                case ENUM_PARAM :
                {
                    const char* v = _v;
                    _p->setTextValue(v);
                }
                    break;
                case BANG_PARAM :
                {
                    _p->setBangValue();
                }
                    break;
                default :
                    return _used;
            }
            _used = true;
        }
        if(_used) break;
    }
    return _used;
}


void receiveJson(JsonObject _json, ParamCollector * _collector) {
    // serializeJson(doc, Serial);
    int ping = _json[F("ping")];
    int push = _json[F("push")];
    if(push != 0) {
        // might need this
    }
    if(ping != 0) {
        for(int i = 0; i < _collector->index; i++) {
            if(_collector->pointers[i]->hasChanged()) {
                // Serial.println(_collector.pointers[i]->getWebUIString());
                // implement with json?
            }
        }
    }
    else {
        const char* addr = _json[F("addr")];
        JsonVariant _v = _json[F("v")];
        if(!dispatchJson(addr, _v, _collector)) {
            Serial.println("[param] err - no matching param");
            serializeJson(_json, Serial);
        }
    }
}

// check the saveType flag and add to JSON document
void saveParams(JsonDocument &_doc, ParamCollector * _collector) {
    JsonArray config = _doc./*to<JsonObject>().*/createNestedArray("config");
    for(int i = 0; i < _collector->index; i++) {
        if(_collector->pointers[i]->saveType == SAVE_ON_REQUEST) {
            JsonObject _param = config.createNestedObject();
            // should throw errors if out of buffer
            jsonifyParam(_param, _collector->pointers[i], false, true);
        }
    }
}

void loadParams(JsonDocument &_doc, ParamCollector * _collector) {
    // Serial.println("attempting to load params");
    JsonArray config = _doc["config"];
    for(JsonObject param : config) {
        const char* addr = param[F("addr")];
        JsonVariant _v = param[F("v")];
        // Serial.println(addr);
        if(!dispatchJson(addr, _v, _collector)) {
            // Serial.println("ERR - no matching param");
            // String _ha;
            // serializeJson(param[F("param")], _ha);
            // Serial.println(_ha);
        }
    }
}
