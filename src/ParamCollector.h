/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#ifndef PARAMCOLLECTOR_H
#define PARAMCOLLECTOR_H

#include "Param.h"

// a helper class to collect parameters
#ifndef MAX_PARAMETER_COUNT
#define MAX_PARAMETER_COUNT 64
#endif

/**
 * @class ParamCollector
 * @brief Class to collect parameters
 *
 * This class is used to collect parameters. It is then used by other static methods to dispatch
 * input to the correct parameter. Multiple collectors can be used for finer grain control over
 * handling different groups of parameters.
 */
class ParamCollector {
    public:
        Param * pointers[MAX_PARAMETER_COUNT];
        int index = 0;
        /**
         * @brief Add a parameter to the collector
         * 
         * @param _p the parameter to add
         */
        void add(Param* _p);
        void addAll(ParamCollector * _pc);
};


#endif
