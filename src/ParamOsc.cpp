/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#include <ParamOsc.h>

bool paramDispatchOSC(MicroOscMessage& _mess, ParamCollector * _collector) {
    for(int j = 0; j < _collector->index; j++) {
        Param * _p = _collector->pointers[j];
        if(_mess.checkOscAddress(_p->address) && _p->writeAccess == true) {
            bool _didUse = true;
            switch(_p->getParamType()) {
                case BOOL_PARAM :
                {
                    bool v = _mess.nextAsInt() != 0;
                    _p->setBoolValue(v);
                }
                    break;
                case FLOAT_PARAM :
                {
                    float v = _mess.nextAsFloat();
                    _p->setFloatValue(v);
                }
                    break;
                case INT_PARAM :
                {
                    int v = _mess.nextAsInt();
                    _p->setIntValue(v);
                }
                    break;
                case COLOR_PARAM :
                {
                    // check type first?
                    byte b1 = _mess.nextAsInt();
                    byte b2 = _mess.nextAsInt();
                    byte b3 = _mess.nextAsInt();
                    long v = (b3 & 0xFF) | ((b2 & 0xFF) << 8) | ((b1 & 0xFF) << 16);
                    _p->setColorValue(v);
                }
                    break;
                case TEXT_PARAM :
                {
                    _p->setTextValue(_mess.nextAsString());
                }
                    break;
                case ENUM_PARAM :
                {
                    _p->setTextValue(_mess.nextAsString());
                }
                    break;
                case BANG_PARAM :
                {
                    _p->setBangValue();
                }
                    break;
                default :
                    _didUse = false;
                    break;
            }
            if(_didUse) {
                return true;
            }
        }
    }
    return false;
}



