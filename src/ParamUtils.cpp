/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2020-02-11
 */


#include "ParamUtils.h"
#include "Param.h"


void ParamLinker::receiveByte(byte _v){
    if(isLinked() && _v != value){
        value = _v;
        if(invert) targetParam->receiveByte(255-_v);
        else targetParam->receiveByte(_v);
    }
}

// ultimately the parameter should decide how to handle bang/bool/num inputs
// enum increase
void ParamLinker::receiveBang(){
    if(isLinked()){
        targetParam->receiveBang();
    }
}

void ParamLinker::receiveBool(bool _b){
    if(isLinked() && _b != value){
        value = _b;
        targetParam->receiveBool(_b);
    }
}



// void ParamLinker::receiveInt(int _i){
//     if(isLinked()){

//     }
// }

// void ParamLinker::receiveFloat(float _f){
//     if(isLinked()){

//     }
// }

bool ParamLinker::needsUpdate(){
    return paramAddress.checkFlag();
}

bool ParamLinker::findParam(ParamCollector * _pc){
    for(int i = 0 ; i < _pc->index; i++){
        Param* _p = _pc->pointers[i];
        // check addresss and grab pointer
        // maybe look for extra value args? like valueA valueB for buttons?
        // 
        if(_p->match(paramAddress.v)) {
            targetParam = _p;
            return true;
            // Serial.printf("[pot] assigned param :\n %s\n", _p->address);
        }
    }
    return false;
}

bool ParamLinker::isLinked(){
    return targetParam != NULL;
}
