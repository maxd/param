/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#include "ParamCollector.h"

void ParamCollector::add(Param* _p){
    if(_p->address){
        pointers[index] = _p;
        index++;
    }
}

void ParamCollector::addAll(ParamCollector* _pc){
    for(int i = 0; i < _pc->index; i++){
        add(_pc->pointers[i]);
    }
}