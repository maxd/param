/*
 * A websocket example for a board with wiznet ethernet connection
 *
 */

#include "Param.h"
#include "ParamCollector.h"
#include "ParamOsc.h"

// #include "OSCMessage.h"

#include <Ethernet.h>
#include <EthernetUdp.h>

// set these pins
#define WIZNET_RESET_PIN 16
#define WIZNET_CS_PIN 15

#define LED_PIN 39

// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;

// EnumParam is ment to work with an enum
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};
// and an array of the values
const int enumValues[] = {FIRST, SECOND, THIRD};
// and the human readable strings
const char* enumStrings[] = {"first", "second", "third"};


// a param collector collects all the parameters
ParamCollector paramCollector;

// OSC
EthernetUDP Udp;
const unsigned int inPort = 8888;
//port numbers
byte mac[] = { 0x40, 0x19, 0xD3, 0xEF, 0x91, 0xDA };
IPAddress ipAddress = {10,0,0,42};


void setup(){
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(9600);
    while(!Serial);
    Serial.println("connected via serial");
    pinMode(LED_PIN, OUTPUT);

    // set parameters
    aFloatParam.set("/a/float", 0, 1, 0.5);
    aFloatParam.setCallback(floatCallback);

    aIntParam.set("/a/int", 0, 255, 100);
    aIntParam.setCallback(intCallback);

    aColorParam.set("/a/color", 0);
    aColorParam.setCallback(colorCallback);

    aBangParam.set("/a/bang");
    aBangParam.setCallback(bangFunction);

    aBoolParam.set("/a/bool", 0);
    aBoolParam.setCallback(ledCallback);

    aTextParam.set("/a/text", "foo");
    aTextParam.setCallback(textCallback);

    aEnumParam.set("/a/enum", enumStrings, enumValues, 3, SECOND);
    aEnumParam.setCallback(enumCallback);

    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);

    // init network
    pinMode(WIZNET_RESET_PIN, OUTPUT);
    digitalWrite(WIZNET_RESET_PIN, LOW);
    pinMode(WIZNET_CS_PIN, OUTPUT);
    digitalWrite(WIZNET_CS_PIN, HIGH);
    digitalWrite(WIZNET_RESET_PIN, HIGH);
    digitalWrite(WIZNET_CS_PIN, LOW);

    // regular ethernet library
    Ethernet.init(WIZNET_CS_PIN);
    // for Ethernet3
    // Ethernet.init(numberOfSocketsYouWant);
    // Ethernet.setCsPin(WIZNET_CS_PIN);

    Ethernet.begin(mac, ipAddress);
    Udp.begin(inPort);

}

void loop(){
    int size = Udp.parsePacket();
    if(size > 0){
        OSCBundle bundleIn;
        while(size--){
            bundleIn.fill(oscUdpSocket.read());
        }
        if(!bundleIn.hasError()){
            // call the param dispatch osc
            paramDispatchOSC(&bundleIn, paramCollector);
        }
    }
}

void update() {
    // do your loop() in here
}

void bangFunction(){
    Serial.println("bang");
}

void ledCallback(bool i){
    Serial.printf("bool : %i\n", i);
    digitalWrite(LED_PIN, i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", _i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(uint16_t _v) {
    Serial.printf("enum : %i %s\n", _v, enumStrings[_v]);
}
