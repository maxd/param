/*
 * A websocket example for a esp8266
 * tested on a Wemos D1 mini Pro
 *
 */

#include "Param.h"
#include "ParamWebsockets.h"

#include <ESP8266WiFi.h>

#define LED_PIN BUILTIN_LED

// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;
// EnumParam is ment to work with an enum
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};
// an array of the values
const int enumValues[] = {FIRST, SECOND, THIRD};
// and the human readable strings
const char* enumStrings[] = {"first", "second", "third"};


// and a param collector
ParamCollector paramCollector;

// ESP8266WiFiMulti WiFiMulti;
 // The SSID (name) of the Wi-Fi network you want to connect to
const char* ssid     = "_______";
// The password of the Wi-Fi network
const char* password = "_______";

void setup(){
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(9600);
    while(!Serial);
    Serial.println("connected via serial");
    pinMode(LED_PIN, OUTPUT);
    // set parameters
    aFloatParam.set("/a/float", 0, 1, 0.5);
    aFloatParam.setCallback(floatCallback);

    aIntParam.set("/a/int", 0, 255, 100);
    aIntParam.setCallback(intCallback);

    aColorParam.set("/a/color", 0);
    aColorParam.setCallback(colorCallback);

    aBangParam.set("/a/bang");
    aBangParam.setCallback(bangFunction);

    aBoolParam.set("/a/bool", 0);
    aBoolParam.setCallback(ledCallback);

    aTextParam.set("/a/text", "foo");
    aTextParam.setCallback(textCallback);

    aEnumParam.set("/a/enum", enumStrings, enumValues, 3, SECOND);
    aEnumParam.setCallback(enumCallback);

    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);

    ////////////////////

    Serial.println();
    WiFi.begin(ssid, password);             // Connect to the network
    Serial.print("Connecting to ");
    Serial.print(ssid); Serial.println(" ...");

    int i = 0;
    while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
        delay(1000);
        Serial.print(++i); Serial.print(' ');
    }

    Serial.println('\n');
    Serial.println("Connection established!");
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer

    paramWebsocketInit(&paramCollector);
}

void loop(){
    updateParamWebsocket();
}


void bangFunction(){
    Serial.println("bang");
}

void ledCallback(bool i){
    digitalWrite(LED_PIN, i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", _i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(int _v) {
    Serial.printf("enum : %i %s\n", _v, enumStrings[_v]);
}
