// testing with arduino uno

#include "Param.h"
#include "ParamMenu.h"

#define BUTTON_PIN 17
// parameters
IntParam maxBrightParam;
IntParam ledCountParam;
IntParam speedParam;
BangParam saveParam;
IntParam mainBrightnessParam;

BangParam modeOneAction;
BangParam modeTwoAction;
BangParam modeTwoNestedAction;
BangParam modeThreeAction;

// collector
ParamCollector paramCollector;
ParamMenu menu;
MenuRenderer renderer;


void setup(){
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    while(!Serial);
    Serial.println("connected via serial");
    // set parameters
    ledCountParam.set("/config/LED/count", 0, 255, 100);
    maxBrightParam.set("/config/LED/maxBrighttoomanycharatcters", 0, 255, 104);
    speedParam.set("/config/speed", 0, 255, 104);
    saveParam.set("/config/save");
    saveParam.setCallback(bangFunction);
    mainBrightnessParam.set("/main/brightness", 0, 255, 200);

    modeOneAction.set("/mode/one/action");
    modeTwoAction.set("/mode/two/action");
    modeTwoNestedAction.set("/mode/two/nest/action");
    modeThreeAction.set("/mode/three/action");

    // add them in the collector
    paramCollector.add(&ledCountParam);
    paramCollector.add(&maxBrightParam);
    paramCollector.add(&speedParam);
    paramCollector.add(&saveParam);
    paramCollector.add(&mainBrightnessParam);

    paramCollector.add(&modeOneAction);
    paramCollector.add(&modeTwoAction);
    paramCollector.add(&modeTwoNestedAction);
    paramCollector.add(&modeThreeAction);

    // build menus :
    // a system where we add sections
    menu.begin(paramCollector);
    Serial.println("----------------------------------------");
    MenuItem * it = &menu.allItems[0];
    recursivePrint(it, 1);
    renderer.begin(32, 6, &Serial);
    Serial.println("booted");
}

void recursivePrint(MenuItem * item, int depth) {
    for(int i = 0 ; i < depth;i++){Serial.print("-");}
    Serial.print(item->name);
    if(item->param != NULL) {
        Serial.printf(" = %s \n", item->param->address);
    }
    else Serial.println();

    if(item->child != NULL) {
        recursivePrint(item->child, depth+2);
    }
    if(item->next != NULL) {
        recursivePrint(item->next, depth);
    }
    if(item->child==NULL && item->next ==NULL) return;
}

void loop(){
    renderer.render(menu);
    Serial.println(checkInput());
    delay(20);
}

void bangFunction(){
    Serial.println("ahah");
}

void ledCallback(bool v){
    digitalWrite(13, v);
}



int checkInput(){
    static int buttonState = 0;
    static int prev = buttonState;
    analogRead(BUTTON_PIN);
    int button_value = analogRead(BUTTON_PIN);
    if(button_value > 900) {
        buttonState = 1;
    }
    else if(button_value > 800) {
        buttonState = 2;
    }
    else if(button_value > 750) {
        buttonState = 3;
    }
    else if(button_value > 600) {
        buttonState = 4;
    }
    if(buttonState != prev) return buttonState;
    else return 0;
}
