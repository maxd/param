/*
 * A websocket example for a board with wiznet ethernet connection
 * NOTE this uses an old Arduino-Websocket fork
 */


#include "Param.h"
#include "ParamCollector.h"
#include "ParamJson.h"

#include "ArduinoJson.h"
// #include "OSCMessage.h"

#include <Ethernet.h>
// use this old library
// https://github.com/maxdee/Arduino-Websocket
#include <WebSocketServer.h>
#include "SdFat.h"
SdFatSdio sd;


#define CONFIG_FILENAME "CONFIG.JSO"
// set these pins
#define WIZNET_RESET_PIN 16
#define WIZNET_CS_PIN 15

#define LED_PIN 39


// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;
BangParam saveParam;


// EnumParam is ment to work with an enum
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};
// and an array of the values
const int enumValues[] = {FIRST, SECOND, THIRD};
// and the human readable strings
const char* enumStrings[] = {"first", "second", "third"};


// a param collector collects all the parameters
ParamCollector paramCollector;

// websocket
EthernetServer serverForSocket(80);
WebSocketServer webSocketServer;

void setup(){
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(9600);
    // the code waits for a serial connection!
    while(!Serial);
    Serial.println("connected via serial");
    pinMode(LED_PIN, OUTPUT);

    // set parameters
    aFloatParam.set("/a/float", 0, 1, 0.5);
    aFloatParam.saveType = SAVE_ON_REQUEST;
    aFloatParam.setCallback(floatCallback);

    aIntParam.set("/a/int", 0, 255, 100);
    aIntParam.saveType = SAVE_ON_REQUEST;
    aIntParam.setCallback(intCallback);

    aColorParam.set("/a/color", 0);
    aColorParam.saveType = SAVE_ON_REQUEST;
    aColorParam.setCallback(colorCallback);

    aBangParam.set("/a/bang");
    aBangParam.saveType = SAVE_ON_REQUEST;
    aBangParam.setCallback(bangFunction);

    aBoolParam.set("/a/bool", 0);
    aBoolParam.saveType = SAVE_ON_REQUEST;
    aBoolParam.setCallback(ledCallback);

    aTextParam.set("/a/text", "foo");
    aTextParam.saveType = SAVE_ON_REQUEST;
    aTextParam.setCallback(textCallback);

    aEnumParam.set("/a/enum", enumStrings, enumValues, 3, SECOND);
    aEnumParam.saveType = SAVE_ON_REQUEST;
    aEnumParam.setCallback(enumCallback);

    saveParam.set("/params/save");
    saveParam.setCallback(saveConfig);

    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);
    paramCollector.add(&saveParam);


    // init network
    pinMode(WIZNET_RESET_PIN, OUTPUT);
    digitalWrite(WIZNET_RESET_PIN, LOW);
    pinMode(WIZNET_CS_PIN, OUTPUT);
    digitalWrite(WIZNET_CS_PIN, HIGH);
    digitalWrite(WIZNET_RESET_PIN, HIGH);
    digitalWrite(WIZNET_CS_PIN, LOW);

    // regular ethernet library
    Ethernet.init(WIZNET_CS_PIN);
    // for Ethernet3
    // Ethernet.init(numberOfSocketsYouWant);
    // Ethernet.setCsPin(WIZNET_CS_PIN);

    byte mac[] = { 0x40, 0x19, 0xD3, 0xEF, 0x91, 0xDA };
    IPAddress ipAddress = {10,0,0,42};
    Ethernet.begin(mac, ipAddress);

    if (!sd.begin()) {
        Serial.println("sdcard initialization failed!");
        while (1);
    }

    load(CONFIG_FILENAME);
}

void loop(){
    // check for new connections
    EthernetClient client = serverForSocket.available();
    // check for websocket request
    if(client.connected() && webSocketServer.handshake(client)){
        Serial.println(F("- ws cnnctn"));
        // on new connection push parameters
        for(int i = 0; i < paramCollector.index; i++) {
            // make blank param object with the add add key
            JsonObject jsonParam = paramJsonDoc.createNestedObject("add");
            // compose a JsonObject with a parameter
            jsonifyParam(jsonParam, paramCollector.pointers[i], true);
            // send the json over websocket
            size_t len = measureJson(paramJsonDoc)+1;
            char tmp[len];
            serializeJson(paramJsonDoc, tmp, len);
            webSocketServer.sendData(tmp);
        }
        // loop while websocket connected
        while(client.connected()){
            // need to get rid of this String, need to look at library
            String _data = webSocketServer.getData();
            if(_data.length() > 0) {
                // deserialize message
                DeserializationError err = deserializeJson(paramJsonDoc, _data);
                if(err){
                    Serial.printf("json fail :%s", err.c_str());
                }
                else {
                    // receive the message
                    JsonObject _obj = paramJsonDoc.as<JsonObject>();
                    receiveJson(_obj, paramCollector);
                }
            }
            update();
        }
        Serial.println(F("! ws dscnct"));
        delay(10);
        client.stop();
    }
    else {
        update();
    }
}

void update() {
    // do your loop() in here
}


void saveConfig(){
    save(CONFIG_FILENAME);
}

void save(const char * _fileName){
    Serial.printf("saving %s\n", _fileName);
    if(sd.exists(_fileName)){
        sd.remove(_fileName);
    }
    File _file = sd.open(_fileName, FILE_WRITE);

    saveParams(paramJsonDoc, paramCollector);
    serializeJson(paramJsonDoc, _file);

    serializeJsonPretty(paramJsonDoc, Serial);
    Serial.println();

    _file.close();
}

void load(const char * _fileName){
    File _file = sd.open(_fileName);
    DeserializationError err = deserializeJson(paramJsonDoc, _file);
    if(err){
        Serial.printf("json fail :%s \n", err.c_str());
    }

    loadParams(paramJsonDoc, paramCollector);
    _file.close();
}




void bangFunction(){
    Serial.println("bang");
}

void ledCallback(bool i){
    Serial.printf("bool : %i\n", i);
    digitalWrite(LED_PIN, i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", _i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(uint16_t _v) {
    Serial.printf("enum : %i %s\n", _v, enumStrings[_v]);
}
