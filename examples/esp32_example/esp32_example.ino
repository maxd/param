/*
 * A websocket example for a ESP32
 * tested 16/04/24 with nodemcu32s and ArduinoESP32 2.0.11
 * this is all available as a library at https://github.com/maxd/esparam
 */

#include <WiFi.h>
#include <WiFiUdp.h>
#include <ESPAsyncWebServer.h>
#include <MicroOsc.h>
#include <MicroOscUdp.h>
#include <Bounce2.h>
#include <FS.h>
#include <LittleFS.h>

#define FORMAT_LittleFS_IF_FAILED true

#include <Param.h>
#include <ParamJson.h>
#include <ParamOsc.h>
#include <ParamUtils.h>

#define BUTTON_PIN 0

AsyncWebServer server(80);
AsyncWebSocket ws("/webparam");

// fill in your wifi credentials
#define WIFI_SSID "aziz-net"
#define WIFI_PSWD "killdash9"

WiFiUDP udpOscSocket;
const int udpOscPort = 3333;
IPAddress sendIp(0, 0, 0, 0);
MicroOscUdp<1024> myMicroOscUdp(&udpOscSocket, sendIp, 9000);

#define LED_PIN BUILTIN_LED


// a param collector to add parameters to
ParamCollector paramCollector;

// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;

// we include params to control the device
BangParam saveButtonParam;
BangParam rebootButtonParam;

// we can use this ParamLinker to connect a button to a parameter
ParamLinker buttonLinker;
Bounce bounceZero = Bounce();

// a enum to use with the EnumParam
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};

// EnumOption is used for the EnumParam.
EnumOption enumOptions[] = {
    {"first",FIRST},
    {"second", SECOND},
    {"third", THIRD}
};


// to save and load from flash
const char * configFileName = "/config.jso";
JsonDocument jsonDoc;

#define PARAM_WEBSOCKET_BUFFER_SIZE 256
char websocketMessageBuffer[PARAM_WEBSOCKET_BUFFER_SIZE];

void websocketEventHandler(
        AsyncWebSocket * server, 
        AsyncWebSocketClient * client, 
        AwsEventType type, 
        void * arg, 
        uint8_t *data, 
        size_t len
    ){
    switch(type){
        case WS_EVT_CONNECT:
            Serial.println("[ws] connected");
            // its usually better to set a flag here and push the parameters
            // from somewhere else not in a callback.
            for(int i = 0; i < paramCollector.index; i++) {
                jsonDoc.clear();
                JsonObject jsonParam = jsonDoc["add"].to<JsonObject>();
                // compose a JsonObject with a parameter
                jsonifyParam(jsonParam, paramCollector.pointers[i], true);
                // send the json over websocket
                size_t len = measureJson(jsonDoc)+1;
                if(len < PARAM_WEBSOCKET_BUFFER_SIZE) {
                    serializeJson(jsonDoc, websocketMessageBuffer, len);
                    client->text(websocketMessageBuffer);    
                }
            }
            break;
        case WS_EVT_DATA:
            {
                DeserializationError err = deserializeJson(jsonDoc, data);
                if(err){
                    Serial.printf("[ws] json fail :%s\n", err.c_str());
                }
                else {
                    // receive the message
                    JsonObject _obj = jsonDoc.as<JsonObject>();
                    receiveJson(_obj, &paramCollector);
                }
            }
            break;
        case WS_EVT_DISCONNECT:
            Serial.println("[ws] disconnected");
            break;
        case WS_EVT_PONG:
            break;
        default:
            break;
    }
}

void setupParameters(){
    aFloatParam.set("/example/numbers/float", 0, 1, 0.5);
    aFloatParam.setCallback(floatCallback);
    aFloatParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aFloatParam);

    aIntParam.set("/example/numbers/int", 0, 255, 100);
    aIntParam.setCallback(intCallback);
    aIntParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aIntParam);

    aColorParam.set("/example/color", 0);
    aColorParam.setCallback(colorCallback);
    aColorParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aColorParam);

    aBangParam.set("/example/buttons/bang");
    aBangParam.setCallback(bangFunction);
    aBangParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aBangParam);

    aBoolParam.set("/example/buttons/bool", 0);
    aBoolParam.setCallback(ledCallback);
    aBoolParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aBoolParam);

    aTextParam.set("/example/text", "foo");
    aTextParam.setCallback(textCallback);
    aTextParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aTextParam);

    aEnumParam.set("/example/enum", SECOND);
    aEnumParam.setOptions(enumOptions, sizeof(enumOptions) / sizeof(EnumOption));
    aEnumParam.setCallback(enumCallback);
    aEnumParam.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&aEnumParam);

    // setup the param linker's inner text param for managing address
    // set it to the bool param to toggle the LED with the button
    buttonLinker.paramAddress.set("/linker/button_zero","/example/buttons/bool");
    buttonLinker.paramAddress.saveType = SAVE_ON_REQUEST;
    paramCollector.add(&buttonLinker.paramAddress);

    saveButtonParam.set("/config/save");
    saveButtonParam.setCallback(saveParamsLittleFS);

    rebootButtonParam.set("/config/reboot");
    rebootButtonParam.setCallback(reboot);

    // add them in the collector
    //
    paramCollector.add(&saveButtonParam);
    paramCollector.add(&rebootButtonParam);

}

void setup(){
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(9600);
    while(!Serial);
    Serial.println("[boot] connected via serial");
    pinMode(LED_PIN, OUTPUT);

    // set parameters
    setupParameters();

    // connect to wifi
    WiFi.begin(WIFI_SSID, WIFI_PSWD);
    Serial.print("[wifi] Connecting to ");
    Serial.println(" ...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print('.');
    }
	
    Serial.println('\n');
    Serial.println("[wifi] connection established!");
    Serial.print("[wifi] IP address:\t");
    Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
    udpOscSocket.begin(udpOscPort);

    ws.onEvent(websocketEventHandler);
    server.addHandler(&ws);
    server.begin();
    // init the file system
    if(!LittleFS.begin()){
        Serial.println("[lfs] An Error has occurred while mounting LittleFS");
        Serial.println("[lfs] may need to format please wait...");
        LittleFS.begin(FORMAT_LittleFS_IF_FAILED);
    }
    else {
        Serial.println("[lfs] done init");
    }
    // load the saves parameters from flash chip
    loadParamsLittleFS();

    bounceZero.attach(BUTTON_PIN, INPUT);
    bounceZero.interval(5);
    Serial.println("[boot] done booting");
}

void myOscMessageParser( MicroOscMessage& receivedOscMessage) {
    paramDispatchOSC(receivedOscMessage, &paramCollector);
}

void loop(){
    // checkOsc
    myMicroOscUdp.onOscMessageReceived( myOscMessageParser );

    // check if we need to find a param to link to the button
    if(buttonLinker.needsUpdate()){
        if(buttonLinker.findParam(&paramCollector)){
            Serial.printf("[input] b0 linked:%s\n",buttonLinker.targetParam->address);
        }
    }
    // update if linker is linked
    if(buttonLinker.isLinked()){
        bounceZero.update();
        if(bounceZero.changed()){
            if(bounceZero.read() == 0){
                buttonLinker.receiveBang();
            }
        }
    }
}

// reboot callback
void reboot(){
    ESP.restart();
}

// save
void saveParamsLittleFS(){
    Serial.printf("[params] saving %s\n", configFileName);
    if(LittleFS.exists(configFileName)){
        LittleFS.remove(configFileName);
    }
    File _file = LittleFS.open(configFileName, FILE_WRITE);
    if(_file){
        // ParamJson has a StaticJsonDocument for us to use
        saveParams(jsonDoc, &paramCollector);
        serializeJson(jsonDoc, _file);
        _file.close();
        // serializeJsonPretty(jsonDoc, Serial);
        Serial.println("\n[params] saved!");
    }
    else {
        Serial.println("[params] failed save params");
    }
}

// load
void loadParamsLittleFS(){
    File _file = LittleFS.open(configFileName);
    if(_file){
        DeserializationError err = deserializeJson(jsonDoc, _file);
        if(err){
            Serial.printf("[params] json fail :%s \n", err.c_str());
        }
        loadParams(jsonDoc, &paramCollector);
        _file.close();
    }
    else {
        Serial.println("[params] failed save params");
    }
}

// These are all the callbacks for the parameters, they are optionnal
void bangFunction(){
    Serial.println("bang!");
}

void ledCallback(bool i){
    digitalWrite(LED_PIN, i);
    Serial.printf("bool : %i\n", i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", (int)_i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(int _v) {
    // erronous on for enum options that arent sequential 0 indexed.
    Serial.printf("enum : %i %s\n", _v, enumOptions[_v].name);
}

